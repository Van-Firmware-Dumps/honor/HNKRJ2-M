#
# Copyright (C) 2024 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from magic device
$(call inherit-product, device/unknown/magic/device.mk)

PRODUCT_DEVICE := magic
PRODUCT_NAME := lineage_magic
PRODUCT_BRAND := Honor
PRODUCT_MODEL := magic
PRODUCT_MANUFACTURER := unknown

PRODUCT_GMS_CLIENTID_BASE := android-unknown

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="magic-user 12 SP1A.210812.016 eng.root.20240827.101747 release-keys"

BUILD_FINGERPRINT := Honor/magic/magic:12/SP1A.210812.016/root08271016:user/release-keys
